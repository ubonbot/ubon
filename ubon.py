#!/usr/bin/python
# -*- coding: iso8859-1 -*-

# ubon.py -- Cliente PyQT de Ubon
# Copyright (C) 2010 Alejandro Rean
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import sys
import os
import ubontalk
import pywapi
from xml.dom import minidom
from PyQt4 import QtCore, QtGui

class Systray(QtGui.QWidget):
	def __init__(self):
		QtGui.QWidget.__init__(self)

		print 'Iniciando Ubon...'

		self.createWindows()	       	

		self.createActions()
		self.createTrayIcon()

		self.trayIcon.show()

		self.say('Hola, es un placer volver a verte.')

		print 'Ubon Iniciado.'

	def createWindows(self):

		self.setWindowModality(QtCore.Qt.NonModal)
	        self.resize(386, 56)
        	self.setMinimumSize(QtCore.QSize(386, 56))
	        self.setMaximumSize(QtCore.QSize(386, 56))

		icon = QtGui.QIcon()
		icon.addPixmap(QtGui.QPixmap("../icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.setWindowIcon(icon)

		self.cajaBotones = QtGui.QDialogButtonBox(self)
		self.cajaBotones.setGeometry(QtCore.QRect(0, 30, 381, 25))
		self.cajaBotones.setOrientation(QtCore.Qt.Horizontal)
		self.cajaBotones.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
		self.cajaBotones.setObjectName("cajaBotones")

		self.textoInput = QtGui.QLineEdit(self)
		self.textoInput.setGeometry(QtCore.QRect(0, 0, 381, 25))
		self.textoInput.setObjectName("textoInput")

		self.pushInput = QtGui.QAction(self)
		self.pushInput.setShortcut(QtCore.Qt.Key_Return)

		self.addAction(self.pushInput)

 		self.connect(self.pushInput, QtCore.SIGNAL("triggered()"), self.accept)		

		self.setWindowTitle("Ubon")

		QtCore.QObject.connect(self.cajaBotones, QtCore.SIGNAL("accepted()"), self.accept)
        	QtCore.QObject.connect(self.cajaBotones, QtCore.SIGNAL("rejected()"), self.reject)
	        QtCore.QMetaObject.connectSlotsByName(self)

	def accept(self) :
		print "accept"
		cadenaInput = self.textoInput.text()
		self.parser(cadenaInput)
		self.textoInput.setText("")

	def reject(self) :
		print "reject"
		self.textoInput.setText("")
		self.hide()

	def closeEvent(self, event):
		print "Cerrado a Tray"
		self.textoInput.setText("")
		self.hide()
		self.say('Para cerrarme haceme clic derecho y selecciona Salir.')
		event.ignore()

	def createActions(self):

		self.aboutAction = QtGui.QAction(self.tr('&Acerca de ...'), self )
		QtCore.QObject.connect(self.aboutAction, QtCore.SIGNAL("triggered()"), self.onAbout)

		self.configAction = QtGui.QAction(self.tr('&Configuracion'), self )
		QtCore.QObject.connect(self.configAction, QtCore.SIGNAL("triggered()"), self.onConfig)

		self.syncAction = QtGui.QAction(self.tr('S&incronizar'), self )
		QtCore.QObject.connect(self.syncAction, QtCore.SIGNAL("triggered()"), self.onSync)
		
		self.quitAction = QtGui.QAction(self.tr("&Salir"), self)
		QtCore.QObject.connect(self.quitAction, QtCore.SIGNAL("triggered()"),
		QtGui.qApp, QtCore.SLOT("quit()"))

		# sincronizado automatico
		self.timer = QtCore.QTimer()
		self.connect( self.timer, QtCore.SIGNAL( 'timeout()'), self.auto_sync )
		self.timer.setInterval( 20000 )	# 20 segundos para la sincronización inicial
		self.timer.setSingleShot( True )
		self.timer.start()

	def createTrayIcon(self):
		self.trayIconMenu = QtGui.QMenu(self)
		self.trayIconMenu.addAction(self.aboutAction)
		self.trayIconMenu.addAction(self.configAction)
		self.trayIconMenu.addAction(self.syncAction)
		self.trayIconMenu.addSeparator()
		self.trayIconMenu.addAction(self.quitAction)

		self.trayIcon = QtGui.QSystemTrayIcon(self)
		self.trayIcon.setIcon(QtGui.QIcon("icon.png"))
		self.trayIcon.setContextMenu(self.trayIconMenu)
		self.trayIcon.setToolTip( "Ubon (Activado)" )

		QtCore.QObject.connect( self.trayIcon, QtCore.SIGNAL('activated(QSystemTrayIcon::ActivationReason)'), self.onActivation )
		QtCore.QObject.connect( self.trayIcon, QtCore.SIGNAL('messageClicked()'), self.show)

	def onActivation(self, reason) :
		print 'onActivation'
		if reason != QtGui.QSystemTrayIcon.Context :
			self.onShowHide()

	def onShowHide(self) :
		print 'click'
		if self.isHidden() :
			self.show()
			self.textoInput.setFocus()
		else :
			self.textoInput.setText("")
			self.hide()

	def parser(self, cadenaInput) :
		print cadenaInput
		listaInput = cadenaInput.split(" ")

		count = 0	
		for item in listaInput :	

			if item == "modulos" :
				self.sayModulos()
				count = count + 1

			if item == "ayuda" :
				self.sayAyuda()
				count = count + 1

			if item == "hola" :
				self.sayHola()
				count = count + 1

			if item == "hora" :
				self.sayHora()
				count = count + 1
				
			if item == "estas" :
				self.sayEstado()
				count = count + 1
				
			if item == "bien" :
				self.sayBien()
				count = count + 1

			if item == "dia" :
				self.sayDia()
				count = count + 1

			if item == "seguro" :
				self.saySi()
				count = count + 1

			if item == "llamas" :
				self.sayNombre()
				count = count + 1
				
			if item == "temperatura" :
				self.sayTemperatura()
				count = count + 1

			if item == "sincronizar" :
				self.onSync()
				count = count + 1

			if item == "chau" :
				self.say('Chau. Nos vemos pronto.')
				quit()

		if count == 0 :
			self.sayNoEntendi()


	def sayAyuda(self) :
		self.say('Por ahora soy medio boludo, solo respondo a los comandos que incluyen las siguientes palabras: ayuda, hola, chau, hora, seguro, llamas, sincronizar y modulos.')

	def sayModulos(self) :
		self.say('En el futuro vas a poder agregarme modulos que me van a acercar nuevas funcionalidades para hacerte la vida digital mas amena.')

	def sayHola(self) :
		self.say('Hola, como estas?')
		
	def sayBien(self) :
		self.say('Me alegro.')
		
	def sayEstado(self) :
		self.say('Yo estoy bien. Funcionando correctamente, y vos?')

	def sayHora(self) :
		self.say('Son las ' + QtCore.QTime.toString(QtCore.QTime.currentTime()))

	def sayDia(self) :
		self.say('Hoy es ' + QtCore.QDate.toString(QtCore.QDate.currentDate()) + '. Faa, como pasa el tiempo...')

	def saySi(self) :
		self.say('Si.')
	
	def sayNombre(self) :
		self.say('Me llamo Ubon y estoy para ayudarte.')
		
	def sayTemperatura(self) :
		google_result = pywapi.get_weather_from_google('buenos+aires')
		temp_c = google_result['current_conditions']['temp_c']
		self.say('En este momento la temperatura en Buenos Aires es de ' + temp_c + ' grados centigrados.')

	def sayNoEntendi(self) :
		self.say('No entendi nada de lo que dijiste. Pedime ayuda y te digo a que comandos respondo.')

	def onAbout(self) :
		print 'onAbout'

	def onConfig(self) :
		print 'onConfig'

	def auto_sync( self ) :
		# luego de realizada la sincronizacion inicial, toma el intervalo configurado
		self.timer.setInterval( 5 * 60000 ) # 60000 = 1 min
		self.timer.setSingleShot( True )

		self.onSync()

		self.timer.start()

	def onSync(self):
		self.say('Acabo de sincronizarme correctamente.')
		print 'Se ha sincronizado Ubon correctamente.'

	def say(self, msg):
		self.trayIcon.showMessage('Ubon', msg)
		ubontalk.speak(msg, self)

app = QtGui.QApplication(sys.argv)
x = Systray()
sys.exit(app.exec_())
